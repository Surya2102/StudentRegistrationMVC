﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BussinessObjects;

namespace DataAccessLayer
{
    public class DataAccess
    {
        SqlConnection conn = new SqlConnection("user id = sa; password=Saisurya21; database=StudentRegistration; data source = CFLNAVALUR4303");
        //To check the usernme and password for student 
        public int LoginFormCheck(LoginFormDetails objbologinformcheck)
        {
            conn.Open();
            int i;
            SqlCommand cmd = new SqlCommand("sp_student_loginformcheck", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserName", objbologinformcheck.UserName);
            cmd.Parameters.AddWithValue("@Password", objbologinformcheck.Password);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.HasRows)
            {
                i = 1;
            }
            else
            {
                i = 0;
            }
            conn.Close();
            return i;
        }
        //To check the username and password for Admin 
        public int StudentAdmin(IsStudentAdmin objbostudentadmin)
        {
            conn.Open();
            int i;
            SqlCommand cmd = new SqlCommand("sp_admin_check", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserName", objbostudentadmin.UserName);
            cmd.Parameters.AddWithValue("@Password", objbostudentadmin.Password);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.HasRows)
            {
                i = 1;
            }
            else
            {
                i = 0;
            }
            conn.Close();
            return i;           
        }
        //Adding student into the database
        public int StudentDetails(StudentRegistrationModel objbostudentdetails)
        {
            conn.Open();

            //passing queries using stored procedure
            SqlCommand cmd = new SqlCommand("sp_student_insertdetails", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Name", objbostudentdetails.FirstName+" "+objbostudentdetails.LastName);
            cmd.Parameters.AddWithValue("@PhoneNumber", objbostudentdetails.PhoneNumber);
            cmd.Parameters.AddWithValue("@DOJ", objbostudentdetails.DOJ);
            cmd.Parameters.AddWithValue("@EmailId", objbostudentdetails.EmailId);
            cmd.Parameters.AddWithValue("@Address", objbostudentdetails.Address);
            cmd.Parameters.AddWithValue("@InchargeId", objbostudentdetails.InchargeId);
            cmd.Parameters.AddWithValue("@CourseId", objbostudentdetails.CourseId);
            cmd.Parameters.AddWithValue("@UserName", objbostudentdetails.UserName);
            cmd.Parameters.AddWithValue("@Password", objbostudentdetails.Password);
            cmd.Parameters.AddWithValue("@IsAdmin", objbostudentdetails.IsAdmin);
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            return i;
        }
        //creating a method for username and asccessing data from businessobject or model layer and Creating the object for businessModellayer
        public string StudentCheck(UserNameCheckModels objbousernamecheck)
        {
            string Username = "";
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_student_usernamecheck", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserName", objbousernamecheck.UserName);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                Username = "User already Existed";
            }
            conn.Close();
            return Username;

        }

        //creating a method for phonemumber and asccessing data from businessobject or model layer and Creating the object for businessModellayer
        public string StudentCheck(PhoneNumberCheckModel objbophonenumbercheck)
        {
            string PhoneNumber = "";
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_student_phonenumbercheck", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PhoneNumber", objbophonenumbercheck.PhoneNumber);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                PhoneNumber = "Phone Number Already Exists";
            }
            conn.Close();
            return PhoneNumber;
        }


        //creating a method for emailid and asccessing data from businessobject or model layer and Creating the object for businessModellayer
        public string StudentCheck(EmailidCheckModel objboemailidcheck)
        {
            string EmailId = "";
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_student_emaildcheck", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmailId", objboemailidcheck.EmailId);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                EmailId = "EmailId Already Exists";             
            }
            conn.Close();
            return EmailId;
        }
        //data for dropdownlist
        public List<admindropdowncheck> getDropdownlist()
        {
            List<admindropdowncheck> objlist = new List<admindropdowncheck>()
            {
                new admindropdowncheck
                {
                    loginid=0,
                    logintype="Student"
                },
                new admindropdowncheck
                {
                    loginid=1,
                    logintype="Admin"
                },
            };
            return objlist;
        }        
        //Get the data from database 
        public List<DisplayData> getList(DisplayData objbodisplaydata)
        {
            
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_student_displaydata", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Username", objbodisplaydata.Name);
            cmd.Parameters.AddWithValue("@IsAdmin", Convert.ToInt32(objbodisplaydata.IsAdmin));
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);

            List<DisplayData> list = new List<DisplayData>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DisplayData data = new DisplayData();
                data.Id = Convert.ToInt32(dt.Rows[i]["Id"]);
                data.Name = dt.Rows[i]["Name"].ToString();
                data.PhoneNumber = dt.Rows[i]["PhoneNumber"].ToString();
                data.EmailId = dt.Rows[i]["EmailId"].ToString();
                data.Address = dt.Rows[i]["Address"].ToString();
                data.IsAdmin = dt.Rows[i]["IsAdmin"].ToString();
                list.Add(data);
            }
            conn.Close();
            return list;

        }
        //Update the data in database
        public int UpdateData(EditData objboupdatedata)
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_updatedata", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Name", objboupdatedata.Name);
            cmd.Parameters.AddWithValue("@PhoneNumber", objboupdatedata.PhoneNumber);
            cmd.Parameters.AddWithValue("@EmailId", objboupdatedata.EmailId);
            cmd.Parameters.AddWithValue("@Address", objboupdatedata.Address);
            cmd.Parameters.AddWithValue("@Id", objboupdatedata.Id);
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            return i;
        }
        public string EditPhoneNumberCheck(EditPhoneNumberCheck objboeditphonenumbercheck)
        {
            string PhoneNumber = "";
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_editphonenumbercheck", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", objboeditphonenumbercheck.Id);
            cmd.Parameters.AddWithValue("@PhoneNumber", objboeditphonenumbercheck.PhoneNumber);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                PhoneNumber = "Phone Number Already Exists";
            }
            conn.Close();
            return PhoneNumber;
        }
        public string EditemailIdCheck(EditEmailIdCheck objboeditemailidcheck)
        {
            string EmailId = "";
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_editemailidcheck", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", objboeditemailidcheck.Id);
            cmd.Parameters.AddWithValue("@EmailId", objboeditemailidcheck.EmailId);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                EmailId = "EmailId Already Exists";
            }
            conn.Close();
            return EmailId;
        }
        public EditData getEditData(DisplayData objboiddata)
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_getData",conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", objboiddata.Id);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            EditData objboedit = new EditData();
            objboedit.Id = Convert.ToInt32(dt.Rows[0]["Id"]);
            objboedit.Name = dt.Rows[0]["Name"].ToString();
            objboedit.PhoneNumber = dt.Rows[0]["PhoneNumber"].ToString();
            objboedit.EmailId = dt.Rows[0]["EmailId"].ToString();
            objboedit.Address = dt.Rows[0]["Address"].ToString();
            conn.Close();
            return objboedit;

        }
        //Delete the student from database
        public int DeleteFunction(DeleteFuncation objbodeletefunction)
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("sp_deletedata", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", objbodeletefunction.Id);
            cmd.Parameters.AddWithValue("@IsAdmin", Convert.ToInt32(objbodeletefunction.IsAdmin));
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            return i;
        }
    }
}
