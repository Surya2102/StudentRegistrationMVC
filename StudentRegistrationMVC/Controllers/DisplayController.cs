﻿using BussinessLogicLayer;
using BussinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentRegistrationMVC.Controllers
{
    [Authorize]
    public class DisplayController : Controller
    {
        BussinessLogic objbllstudent = new BussinessLogic();

        // GET: Disaplay
        /// <summary>
        /// Display all the registered student data from database  
        /// </summary>
        public ActionResult DisplayData()
        {
            DisplayData objbodisplaydata = new DisplayData();
            objbodisplaydata.Name = Session["UserName"].ToString();
            objbodisplaydata.IsAdmin = Session["IsAdmin"].ToString();
            var list = objbllstudent.getList(objbodisplaydata);
            return View(list);
        }
        /// <summary>
        ///  The details are edited only by Admin .It will validate user is admin or not then allows to edit the details
        /// </summary>
        public ActionResult EditData(DisplayData objmodel)
        {
            if (Convert.ToInt32(Session["IsAdmin"]) == 1)
            {
                var objboeditdata = objbllstudent.getEditData(objmodel);
                return View(objboeditdata);
            }
            else
            {
                return RedirectToAction("DisplayData");
            }
        }
        /// <summary>
        /// This action method will update the edited details and refreshes page to load the updated data 
        /// </summary>
        [HttpPost]
        public ActionResult EditData(EditData objeditdata)
        {
            EditPhoneNumberCheck objboeditphonenmumbercheck = new EditPhoneNumberCheck();
            objboeditphonenmumbercheck.Id = objeditdata.Id;
            objboeditphonenmumbercheck.PhoneNumber = objeditdata.PhoneNumber;
            string phonenumber = objbllstudent.EditPhoneNumberCheck(objboeditphonenmumbercheck);

            EditEmailIdCheck objboeditemailidcheck = new EditEmailIdCheck();
            objboeditemailidcheck.Id = objeditdata.Id;
            objboeditemailidcheck.EmailId = objeditdata.EmailId;

            string emailid = objbllstudent.EditemailIdCheck(objboeditemailidcheck);
            if(phonenumber != "")
            {
                ViewBag.Message = "Phone Number Already Exists!..";
            }
            else if(emailid != "")
            {
                ViewBag.Message = "EmailId Already Exists!..";
            }
            else
            {
                int i = objbllstudent.UpdateData(objeditdata);
                if (i == 1)
                {
                    ViewBag.Message = "Data Updated Successfully";
                    ModelState.Clear();
                    return View();
                }
                else
                {
                    ViewBag.Message = "Error Occured..Try Again";
                    return View();
                }
            }
            return View();
        }
        /// <summary>
        /// This action method will allow admin to delete the student or admin details from the database
        /// </summary>
        public ActionResult Delete(DeleteFuncation objbodeletefunction)
        {
            int i = objbllstudent.DeleteFunction(objbodeletefunction);
            if (i == 1 || i == 2)
            {
                ViewBag.Message = "Record Deleted Successfully!..";
            }
            else
            {
                ViewBag.Message = "Record Not Deleted!..";

            }
            return RedirectToAction("DisplayData");
        }
    }
}




















//objboeditdata.Id = objmodel.Id;
//objboeditdata.Name = objmodel.Name;
//objboeditdata.EmailId = objmodel.EmailId;
//objboeditdata.PhoneNumber = objmodel.PhoneNumber;
//objboeditdata.Address = objmodel.Address;