﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using BussinessObjects;
using BussinessLogicLayer;
using System.Data.SqlClient;
using System.Web.UI;
using System.Net.Http;

namespace StudentRegistrationMVC.Controllers
{
    public class HomeController : Controller
    {
        BussinessLogic objbllstudent = new BussinessLogic();
        private readonly IAuth _auth;
        /// <summary>
        /// dependency injection
        /// </summary>
        public HomeController(IAuth auth)
        {
            _auth = auth;
        }
        
        /// <summary>
        /// Student Login action method and 
        /// </summary>
        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.list = objbllstudent.getDropdownlist();
            return View();
        }
        public string Result()
        {
            return "Successfully login";
        }
        
        /// <param name="objbologinform"></param>
        /// <returns></returns>
        [HttpPost]
        /// <summary>
        /// After student entered his username and password it will come to this login action method where credentials are validated
        /// </summary>
        public ActionResult Login(LoginFormDetails objbologinform)
        {
            ViewBag.list = objbllstudent.getDropdownlist();
            if (ModelState.IsValid)
            {                
                var stype = objbologinform.UserType;
                if (Convert.ToInt32(stype) == 1)
                {                  
                    IsStudentAdmin objbostudentadmin = new IsStudentAdmin();
                    objbostudentadmin.UserName = objbologinform.UserName;
                    objbostudentadmin.Password = objbologinform.Password;
                    int studentadmin = objbllstudent.StudentAdmin(objbostudentadmin);
                    if (studentadmin == 1)
                    {                        
                        _auth.DoAuth(objbologinform.UserName, false);
                        Session["UserName"] = objbologinform.UserName;
                        Session["IsAdmin"] = stype;
                        ViewBag.Message = "Successfull Login";

                        return RedirectToAction("DisplayData", "Display");                                                                       
                    }
                    else
                    {                        
                        ViewBag.Message = "Invalid Credentials";

                    }
                    ModelState.Clear();


                }
                else
                {                    
                    LoginFormDetails objbologinforms = new LoginFormDetails();
                    objbologinforms.UserName = objbologinform.UserName;
                    objbologinforms.Password = objbologinform.Password;
                    int studentlogin = objbllstudent.LoginFormCheck(objbologinform);
                    if (studentlogin == 1)
                    {                       
                        _auth.DoAuth(objbologinform.UserName, false);
                        Session["UserName"] = objbologinform.UserName;
                        Session["IsAdmin"] = stype;
                        ModelState.Clear();
                        return RedirectToAction("DisplayData", "Display");
                    }
                    else
                    {
                        ViewBag.Message = "Invalid Credentials";
                    }
                    ModelState.Clear();
                }
            }
            return View();
        }

        /// <summary>
        /// Student registration form action method 
        /// </summary>
        [HttpGet]
        public ActionResult StudentRegistration()
        {
            ViewBag.list = objbllstudent.getDropdownlist();         
            return View();
        }

        /// <summary>
        /// This action method will add new user to database and redirect to login form 
        /// </summary>
        [HttpPost]
        public ActionResult StudentRegistration(StudentRegistrationModel objbostudentregistration)
        {
            ViewBag.list = objbllstudent.getDropdownlist();           
            if (ModelState.IsValid)
            {              
                UserNameCheckModels objbousernamecheck = new UserNameCheckModels();
                objbousernamecheck.UserName = objbostudentregistration.UserName;
                //assining businesslogiclayer object and assigning method of businesslogiclayer to businessobject or model layer
                string username = objbllstudent.StudentCheck(objbousernamecheck);

                PhoneNumberCheckModel objbophonenumbercheck = new PhoneNumberCheckModel();
                objbophonenumbercheck.PhoneNumber = objbostudentregistration.PhoneNumber;
                string phonenumber = objbllstudent.StudentCheck(objbophonenumbercheck);

                EmailidCheckModel objboemailidcheck = new EmailidCheckModel();
                objboemailidcheck.EmailId = objbostudentregistration.EmailId;
                string emailid = objbllstudent.StudentCheck(objboemailidcheck);
                if (username != "")
                {
                    ViewBag.Message = "User Already Existed!..";

                }
                else if (phonenumber != "")
                {
                    ViewBag.Message = "Phone Number Already Exists!..";

                }
                else if (emailid != "")
                {
                    ViewBag.Message = "EmailId Already Exists!..";

                }
                else
                {
                    //assining businesslogiclayer object and assigning method of businesslogiclayer to businessobject or model layer
                    int i = objbllstudent.StudentDetails(objbostudentregistration);
                    //int j = objbllstudent.StudentIsAdmin(objbostudentisadmin);
                    if (i == 1 || i == 2)
                    {
                        ViewBag.Message = "Student Details Saved Sccessfully!..";
                        ModelState.Clear();

                    }
                    else
                    {
                        ViewBag.Message = "Failed to insert!..";

                    }
                }
            }
            return View();

        }
        /// <summary>
        /// Logout action method will remove all the cookies and redirect to login action method 
        /// </summary>
        public ActionResult Logout()
        {
            _auth.dosignout();
            Session.Remove("UserName");
            Session.RemoveAll();
            return RedirectToAction("Login");
        }
    }
}