﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessObjects;
using DataAccessLayer;
using System.Data.SqlClient;
using System.Data;

namespace BussinessLogicLayer
{
    public class BussinessLogic
    {
        DataAccess objdalstudent = new DataAccess();
        public int LoginFormCheck(LoginFormDetails objbologinformcheck)
        {
            int loginformcheckresult = objdalstudent.LoginFormCheck(objbologinformcheck);
            return loginformcheckresult;
        }
        public int StudentAdmin(IsStudentAdmin objbostudentadmin)
        {
            int StudentAdminresult = objdalstudent.StudentAdmin(objbostudentadmin);
            return StudentAdminresult;
        }
        public int StudentDetails(StudentRegistrationModel objbostudentdetails)
        {

            //assining dataaccesslayer object to businessobject or model layer
            int studentdetails = objdalstudent.StudentDetails(objbostudentdetails);
            return studentdetails;
        }
        //using same (data access layer method) in businesslogiclayer
        public string StudentCheck(UserNameCheckModels objbousernamecheck)
        {

            //assining dataaccesslayer object to businessobject or model layer
            string studentcheckusername = objdalstudent.StudentCheck(objbousernamecheck);
            return studentcheckusername;
        }

        //using same (data access layer method) in businesslogiclayer
        public string StudentCheck(PhoneNumberCheckModel objbophonenumbercheck)
        {

            //assining dataaccesslayer object to businessobject or model layer
            string studentcheckphonenumber = objdalstudent.StudentCheck(objbophonenumbercheck);
            return studentcheckphonenumber;
        }

        //using same (data access layer method) in businesslogiclayer
        public string StudentCheck(EmailidCheckModel objboemailidcheck)
        {

            //assining dataaccesslayer object to businessobject or model layer
            string studentcheckemailid = objdalstudent.StudentCheck(objboemailidcheck);
            return studentcheckemailid;
        }
        public List<admindropdowncheck> getDropdownlist()
        {
            var list = objdalstudent.getDropdownlist();
            return list;
        }
      
        public List<DisplayData> getList(DisplayData objbodisplaydata)
        {
            var list = objdalstudent.getList(objbodisplaydata);
            return list;
        }
        public int UpdateData(EditData objboupdatedata)
        {
            int update = objdalstudent.UpdateData(objboupdatedata);
            return update;
        }
        public string EditPhoneNumberCheck(EditPhoneNumberCheck objboeditphonenumbercheck)
        {
            string phonenumber = objdalstudent.EditPhoneNumberCheck(objboeditphonenumbercheck);
            return phonenumber;
        }
        public string EditemailIdCheck(EditEmailIdCheck objboeditemailidcheck)
        {
            string emailid = objdalstudent.EditemailIdCheck(objboeditemailidcheck);
            return emailid;
        }
        public int DeleteFunction(DeleteFuncation objbodeletefunction)
        {
            int delete = objdalstudent.DeleteFunction(objbodeletefunction);
            return delete;
        }
        public EditData getEditData(DisplayData objboiddata)
        {
            var objboedit = objdalstudent.getEditData(objboiddata);
            return objboedit;
        }

    }
}
