﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer
{
    public interface IAuth
    {
        void DoAuth(string username, bool remember);
        void dosignout();
    }
}
