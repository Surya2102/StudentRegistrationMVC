﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace BussinessLogicLayer
{
    public class Auth : IAuth
    {
        public void DoAuth(string username, bool remember)
        {
            FormsAuthentication.SetAuthCookie(username, remember);
        }

        public void dosignout()
        {
            FormsAuthentication.SignOut();
        }
    }
}
