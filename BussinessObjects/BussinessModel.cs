﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace BussinessObjects
{
    public class LoginFormDetails
    {
        [Required]
        public string UserType { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
    public class IsStudentAdmin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class StudentRegistrationModel
    {

        //creating get and set method the name of the method need to be same as database table column name
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Password \"{0}\" must have {2} character", MinimumLength = 8)]
        [RegularExpression(@"^([a-zA-Z0-9@*#]{8,15})$", ErrorMessage = "Password must contain: Minimum 8 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character")]
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [Display(Name ="Confirm Password")]
        public string ConfirmPasswords { get; set; }
        [Required]
        [EmailAddress]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail id is not valid")]
        public string EmailId { get; set; }
        [Required]
        [Display(Name ="Contact Number")]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Please Enter Valid Mobile Number.")]
        public long PhoneNumber { get; set; }
        [Required]
        public Gender Gender { get; set; }
        [Required]
        public Course CourseId { get; set; }
        [Required]
        public Incharge InchargeId { get; set; }
        [Required]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public string DOB { get; set; }
        [Required]
        [Display(Name = "Date of Joining")]
        [DataType(DataType.Date)]
        public string DOJ { get; set; }
        [Required]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Entered must be numeric")]
        public string Fee { get; set; }
        [Required]
        public string HomeTown { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [Display(Name ="Student Type")]
        public string IsAdmin { get; set; }
    }
    public enum Gender
    {
        Male=1,
        Female=2,
        Others=3,
    }
    public enum Course
    {
        CSE=101,
        ECE=102,
        EEE=105,
        IT=104,
        MECH=103,
        CIVIL=106,
        Others=107,
    }
    public enum Incharge
    {
        CSE_1=1,
        ECE_2=4,
        EEE_3=5,
        IT_4=3,
        MECH_5=2,
        CIVIL_6=6,
        Others_7=7,
    }
    //creating username get and set method the method name need to be same as in database column name
    public class UserNameCheckModels
    {
        public string UserName { get; set; }
    }

    //creating phonenumber get and set method the method name need to be same as in database column name
    public class PhoneNumberCheckModel
    {
        public long PhoneNumber { get; set; }

    }
    //creating emailid get and set method the method name need to be same as in database column name
    public class EmailidCheckModel
    {
        public string EmailId { get; set; }
    }
    public class admindropdowncheck
    {
        public int loginid { get; set; }
        public string logintype { get; set; }
    }
    public class DisplayData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public string IsAdmin { get; set; }
    }
    public class EditData
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string EmailId { get; set; }
        [Required]
        public string Address { get; set; }

    }
    public class EditPhoneNumberCheck
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class EditEmailIdCheck
    {
        public int Id { get; set; }
        public string EmailId { get; set; }
    }
    public class DeleteFuncation
    {
        public int Id { get; set; }
        public int IsAdmin { get; set; }
    }
}
