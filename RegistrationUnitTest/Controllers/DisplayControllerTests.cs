﻿using BussinessObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentRegistrationMVC.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace StudentRegistrationMVC.Controllers.Tests
{
    [TestClass()]
    public class DisplayControllerTests
    {
        [TestMethod()]
        public void DisplayDataTest()
        {
            //Arrange
            /*DisplayController objdisplaycontroller = new DisplayController();
            DisplayData objbodisplaydata = new DisplayData();
            objbodisplaydata.Name = "surya";
            objbodisplaydata.IsAdmin = "1";*/

            //Act
            //var result = objdisplaycontroller.DisplayData() as ViewResult;

            //Assert
            //Assert.IsNotNull(result);

        }

        [TestMethod()]
        public void EditDataTestPhoneNumber()
        {
            //Arrange
            DisplayController objdisplaycontroller = new DisplayController();
            EditData objeditdata = new EditData();
            objeditdata.Id = 31;
            objeditdata.Name = "harsah";
            objeditdata.PhoneNumber = "9312415110";
            objeditdata.EmailId = "ram@gmail.com";
            objeditdata.Address = "asdfgh";


            //Act
            var result = objdisplaycontroller.EditData(objeditdata) as ViewResult;

            //Assert
            Assert.AreEqual("Phone Number Already Exists!..", result.ViewBag.Message);
        }

        [TestMethod()]
        public void EditDataTestEmailId()
        {
            //Arrange
            DisplayController objdisplaycontroller = new DisplayController();
            EditData objeditdata = new EditData();
            objeditdata.Id = 31;
            objeditdata.Name = "harsah";
            objeditdata.PhoneNumber = "9241210000";
            objeditdata.EmailId = "suu@gmail.com";
            objeditdata.Address = "asdfgh";


            //Act
            var result = objdisplaycontroller.EditData(objeditdata) as ViewResult;

            //Assert
            Assert.AreEqual("EmailId Already Exists!..", result.ViewBag.Message);
        }

        [TestMethod()]
        public void EditDataTestUpdated()
        {
            //Arrange
            DisplayController objdisplaycontroller = new DisplayController();
            EditData objeditdata = new EditData();
            objeditdata.Id = 31;
            objeditdata.Name = "ramesh";
            objeditdata.PhoneNumber = "9241210000";
            objeditdata.EmailId = "ramesh@gmail.com";
            objeditdata.Address = "asdfglkjh";


            //Act
            var result = objdisplaycontroller.EditData(objeditdata) as ViewResult;

            //Assert
            Assert.AreEqual("Data Updated Successfully", result.ViewBag.Message);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            //Arrange
            DisplayController objdisplaycontroller = new DisplayController();
            DeleteFuncation objdeletefunction = new DeleteFuncation();
            objdeletefunction.Id = 1030;
            objdeletefunction.IsAdmin = 1;

            //Act
            var result = objdisplaycontroller.Delete(objdeletefunction) as ViewResult;

            //Assert
            Assert.IsNull(result);
        }
    }
}