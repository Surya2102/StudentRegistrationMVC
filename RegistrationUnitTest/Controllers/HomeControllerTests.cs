﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentRegistrationMVC.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using StudentRegistrationMVC;
using BussinessObjects;
using BussinessLogicLayer;
using DataAccessLayer;
using System.Web.Security;
using System.Net.Http;
using Moq;
using System.Web;


namespace StudentRegistrationMVC.Controllers.Tests
{
    [TestClass()]
    public class HomeControllerTests
    {
        [TestMethod()]
        public void LoginTest()
        {
            //Arrange
            HomeController objhomeController = new HomeController(new Auth());

            //Act
            var result = objhomeController.Login() as ViewResult;

            //Assert
            Assert.IsNotNull(result);

        }

        [TestMethod()]
        public void StudentRegistrationTest()
        {
            //Arrange
            HomeController objhomeController = new HomeController(new Auth());
            /*string userName = "surya";*/

            //Act
            var result = objhomeController.StudentRegistration() as ViewResult;

            //Assert
            Assert.IsNotNull(result);

        }

        [TestMethod()]
        public void StudentRegistrationTest1()
        {
            //Arrange
            HomeController objhomecontroller = new HomeController(new Auth());
            StudentRegistrationModel objbostudentregistration = new StudentRegistrationModel();
            objbostudentregistration.FirstName = "hariram";
            objbostudentregistration.LastName = "husu";
            objbostudentregistration.UserName = "hram";
            objbostudentregistration.Password = "susususu";
            objbostudentregistration.ConfirmPasswords = "susususu";
            objbostudentregistration.EmailId = "ram@gmail.com";
            objbostudentregistration.PhoneNumber = 9078123110;
            objbostudentregistration.Gender = Gender.Male;
            objbostudentregistration.CourseId = Course.ECE;
            objbostudentregistration.InchargeId = Incharge.ECE_2;
            objbostudentregistration.DOB = "2000-12-21";
            objbostudentregistration.DOJ = "2022-08-06";
            objbostudentregistration.Fee = "12345";
            objbostudentregistration.HomeTown = "qwer";
            objbostudentregistration.Address = "asdfgh";
            objbostudentregistration.IsAdmin = "0";
            //Act
            var result = objhomecontroller.StudentRegistration(objbostudentregistration) as ViewResult;
            Assert.AreEqual("Student Details Saved Sccessfully!..", result.ViewBag.Message);
        }

        [TestMethod()]
        public void LoginTest1()
        {
            //Arrange
            HomeController objhomecontroller = new HomeController(new Auth());
            LoginFormDetails objloginfromdetails = new LoginFormDetails();

            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);

            objloginfromdetails.UserType = "1";
            objloginfromdetails.UserName = "surya";
            objloginfromdetails.Password = "sai";
            objhomecontroller.ControllerContext = context.Object;
            
            //Act
            var result = objhomecontroller.Login(objloginfromdetails) as ViewResult;
            
            Assert.IsNull(result);

        }
    }
}