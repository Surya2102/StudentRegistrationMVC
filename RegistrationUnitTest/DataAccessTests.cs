﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessObjects;

namespace DataAccessLayer.Tests
{
    [TestClass()]
    public class DataAccessTests
    {
        [TestMethod()]
        public void LoginFormCheckTest()
        {
            //Arrange
            DataAccess objdataaccess = new DataAccess();
            LoginFormDetails objloginformdetails = new LoginFormDetails();
            objloginformdetails.UserName = "hari";
            objloginformdetails.Password = "hari";

            //Act
            var result = objdataaccess.LoginFormCheck(objloginformdetails);

            //Assert
            Assert.AreEqual(1, result);

        }

        [TestMethod()]
        public void StudentAdminTest()
        {
            //Arrange
            DataAccess objdataaccess = new DataAccess();
            IsStudentAdmin objisstudentadmin = new IsStudentAdmin();
            objisstudentadmin.UserName = "surya";
            objisstudentadmin.Password = "sai";

            //Act
            var result = objdataaccess.StudentAdmin(objisstudentadmin);

            //Assert
            Assert.AreEqual(1, result);

        }

        [TestMethod()]
        public void StudentDetailsTest()
        {
            //Arrange
            DataAccess objdataaccess = new DataAccess();
            StudentRegistrationModel objstudentregistrationmodel = new StudentRegistrationModel();
            objstudentregistrationmodel.FirstName = "gopi";
            objstudentregistrationmodel.LastName = "ram";
            objstudentregistrationmodel.PhoneNumber = 9503302100;
            objstudentregistrationmodel.DOJ = "2022-07-12";
            objstudentregistrationmodel.EmailId = "gopi@gmail.com";
            objstudentregistrationmodel.Address = "asdfgh";
            objstudentregistrationmodel.InchargeId = Incharge.EEE_3;
            objstudentregistrationmodel.CourseId = Course.EEE;
            objstudentregistrationmodel.UserName = "gopi";
            objstudentregistrationmodel.Password = "gopigopi";
            objstudentregistrationmodel.IsAdmin = "1";

            //Act
            var result = objdataaccess.StudentDetails(objstudentregistrationmodel);

            //Assert
            Assert.AreEqual(2, result);
        }

        [TestMethod()]
        public void StudentCheckTest()
        {
            //Arrange
            DataAccess objdataaccess = new DataAccess();
            UserNameCheckModels objbousernamecheck = new UserNameCheckModels();
            objbousernamecheck.UserName = "surya";

            //Act
            var result = objdataaccess.StudentCheck(objbousernamecheck);

            //Assert
            Assert.AreEqual("User already Existed", result);

        }

        [TestMethod()]
        public void StudentCheckTest1()
        {
            //Arrange
            DataAccess objdataaccess = new DataAccess();
            PhoneNumberCheckModel objbophonenumbercheck = new PhoneNumberCheckModel();
            objbophonenumbercheck.PhoneNumber = 9341212022;

            //Act
            var result = objdataaccess.StudentCheck(objbophonenumbercheck);

            //Assert
            Assert.AreEqual("Phone Number Already Exists", result);

        }

        [TestMethod()]
        public void StudentCheckTest2()
        {
            //Arrange
            DataAccess objdataaccess = new DataAccess();
            EmailidCheckModel objboemailidcheck = new EmailidCheckModel();
            objboemailidcheck.EmailId = "s@gmail.com";

            //Act
            var result = objdataaccess.StudentCheck(objboemailidcheck);

            //Assert
            Assert.AreEqual("EmailId Already Exists", result);

        }

        [TestMethod()]
        public void getListTest()
        {
            //Arrange
            DataAccess objdataaccess = new DataAccess();
            DisplayData objbodisplaydata = new DisplayData();
            objbodisplaydata.Name = "surya";
            objbodisplaydata.IsAdmin = "1";

            //Act
            var result = objdataaccess.getList(objbodisplaydata);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void UpdateDataTest()
        {
            //Arrange
            DataAccess objdataaccess = new DataAccess();
            EditData objboupdatedata = new EditData();
            objboupdatedata.Id = 2;
            objboupdatedata.Name = "MallaReddy";
            objboupdatedata.PhoneNumber = "6304141431";
            objboupdatedata.EmailId = "mallareddy@gmail.com";
            objboupdatedata.Address = "asdfg";

            //Act
            var result = objdataaccess.UpdateData(objboupdatedata);

            //Assert
            Assert.AreEqual(1, result);
        }
    }
}